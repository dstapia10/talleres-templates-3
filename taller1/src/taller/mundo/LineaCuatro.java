package taller.mundo;

import java.util.ArrayList;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;
	
	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;
	
	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;
		
	/**
	 * El número del jugador en turno
	 */
	private int turno;
	
	/**
	 * Crea una nueva instancia del juego
	 */
	
	/**
	 * Indica si hay algun ganador
	 */
	private boolean victoria;
	
	/**
	 * Indica el ultimo jugdor que jugo
	 */
	private String ultimo;
	
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="   ";	
			}
		}
		jugadores = pJugadores;
//		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
		victoria = false;		
		ultimo="nadie";
	}
	
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}
	
	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	
	public String darAtacante()
	{
		return atacante;
	}
	
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		if(victoria() || !tableroConEspacio())return true;		
		return false;
	}
	
	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		//TODO
		while(true)
		{	
			int aleatorio = (( int ) ( Math.random( ) * (tablero[0].length) ))+1;
			if(registrarJugada(aleatorio-1))break;
		}
	}
	
	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		//TODO
		
		for(int i=tablero.length-1 ; i>=0 ; i--)
		{
			if((tablero[i][col]).equals("   "))
			{
				tablero[i][col]=" "+jugadores.get(turno).darSimbolo()+" ";
				ultimo = atacante;
				if(turno+1<jugadores.size())
				{
					turno++;
				}
				else
				{
					turno=0;
				}
				atacante = jugadores.get(turno).darNombre();
				return true;			
			}				
		}		
		return false;
	}
	
	/**
	 * Verifica si alguien ya gano
	 * @return true algien ya gano, false de lo contrario
	 */
	public boolean victoria()
	{
		for(int i=0 ; i<tablero.length ; i++)
		{
			for(int j=0 ; j<tablero[0].length ; j++)
			{	
				if(!tablero[i][j].equals("   "))
				{					
					//Filas				
					if(i+3<tablero.length)
					{
						if(tablero[i][j].equals(tablero[i+1][j]))
						{
							if(tablero[i][j].equals(tablero[i+2][j]))
							{
								if(tablero[i][j].equals(tablero[i+3][j]))
								{
									victoria = true;
									return true;
								}
							}
						}
					}
															
					//Columnas
					if(j+3<tablero[0].length)
					{
						if(tablero[i][j].equals(tablero[i][j+1]))
						{
							if(tablero[i][j].equals(tablero[i][j+2]))
							{
								if(tablero[i][j].equals(tablero[i][j+3]))
								{
									victoria = true;
									return true;
								}
							}
						}
					}			
					
					//diagonal desde la esquina izquierda superior
					if(i+3<tablero.length && j+3<tablero[0].length)
					{
						if(tablero[i][j].equals(tablero[i+1][j+1]))
						{
							if(tablero[i][j].equals(tablero[i+2][j+2]))
							{
								if(tablero[i][j].equals(tablero[i+3][j+3]))
								{
									victoria = true;
									return true;
								}
							}
						}
					}
					
										
					//diagonal desde la esquina izquierda inferior	
					if(i-3>=0 && j+3<tablero[0].length)
					{
						if(tablero[i][j].equals(tablero[i-1][j+1]))
						{
							if(tablero[i][j].equals(tablero[i-2][j+2]))
							{
								if(tablero[i][j].equals(tablero[i-3][j+3]))
								{
									victoria = true;
									return true;
								}
							}
						}	
					}
								
				}
			}
		}		
		return false;
	}
	
	/**
	 * Devuelve si algien ya gano
	 * @return true si alguien ya gano, false de lo contrario
	 */
	public boolean darVictoria()
	{
		return victoria;
	}
	
	public String darUltimo()
	{
		return ultimo;
	}
	
	/**
	 * Verifica si el tablero tiene espacio
	 * @return true si el tabelro tiene espacio, false de lo contrario
	 */
	public boolean tableroConEspacio()
	{
		for(int i=0 ; i<tablero.length ; i++)
		{	
			for(int j=0 ; j<tablero.length ; j++)
			{
				if(tablero[i][j].equals("   "))return true;
			}		
		}
		return false;
	}
	
	/**
	 * verifica si una columna tiene espacio
	 * @return true si la columna tiene espacio, false de lo contrario
	 */
	public boolean columnaConEspacio(int col)
	{
		for(int i=0 ; i<tablero.length ; i++)
		{	
			if(tablero[i][col].equals("   "))return true;		
		}
		return false;
	}

}
