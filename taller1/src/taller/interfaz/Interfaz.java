package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;
	
	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;
	
	/**
	 * Lista de jugadores
	 */
	private ArrayList<Jugador> jugadores;
		
	/**
	 * Informacion de tablero
	 */
	private String[][] tab;
	
	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("");	
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}
		}
	}
	
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println(" ");
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");			
    }
	
	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
       //TODO
		
		//valores inicilas
		int filas = 0;
		int columnas = 0;
		int numeroJugadores = 0;
			
		//pide filas
		while (true)
		{
			try
			{
				System.out.println("");
				System.out.println("¿Numero de filas (minimo 4)?");
				filas = Integer.parseInt(sc.next());
				if(filas>3)break;
			}
			catch (NumberFormatException e)
			{
				System.out.println("Por favor ingrese un numero de filas valido");
				continue;
			}
		}
		
		//pide columnas
		while (true)
		{
			try
			{
				System.out.println("");
				System.out.println("¿Numero de columnas (minimo 4)?");
				columnas = Integer.parseInt(sc.next());
				if(columnas>3)break;
			}
			catch (NumberFormatException e)
			{
				System.out.println("Por favor ingrese un numero de columnas valido");
				continue;
			}
		}
		
		//pide numero jugadores
		while (true)
		{
			try
			{
				System.out.println("");
				System.out.println("¿Cuantos jugadores van a jugar (minimo 2)?");
				numeroJugadores = Integer.parseInt(sc.next());
				if(numeroJugadores>1)break;
			}
			catch (NumberFormatException e)
			{
				System.out.println("Por favor ingrese un numero de jugadores valido");
				continue;
			}
		}

		jugadores = new ArrayList<Jugador>();
		jugadores.removeAll(jugadores);
		for(int i =0 ; i<numeroJugadores ; i++)
		{
			String nombre = "incial";
			String simbolo = "inicial";
			
			//pide nombre
			while (true)
			{
				try
				{
					System.out.println("");
					System.out.println("¿Que nombre quiere usar el jugador "+(i+1)+" ?");
					nombre = sc.next();
					boolean bre=false;
					if(jugadores.size()==0)break;
					else
					{
						for(int n=0; n<jugadores.size() ; n++)
						{
							if(!jugadores.get(n).darNombre().equals(nombre))
							{
								bre=true;
								break;
							}
						}		
					}
					if(bre)break;
				}
				catch (NumberFormatException e)
				{
					System.out.println("por favor igrese un nombre valido difernte a los ya existentes");
					continue;
				}
			}

			//pide simbolo
			while (true)
			{
				try
				{
					System.out.println("");
					System.out.println("¿Que simbolo desea usar el jugador "+(i+1)+" (numero de un digito o una letra)?");
					simbolo = sc.next();
					boolean bre = false;
					if(jugadores.size()==0 && (esMayuscula(simbolo) 
							|| esMinuscula(simbolo) || esNumeroUnDigito(simbolo))
							)break;
					else
					{
						for(int n=0; n<jugadores.size() ; n++)
						{
							if(!jugadores.get(n).darSimbolo().equals(simbolo) && 
									(esMayuscula(simbolo) || esMinuscula(simbolo) 
									|| esNumeroUnDigito(simbolo)))
							{
								bre = true;
								break;				
							}
						}		
					}
					if(bre)break;
				}
				catch (NumberFormatException e)
				{
					System.out.println("Por favor, seleccione un simbolo valido");
					continue;
				}
			}		
			jugadores.add( new Jugador(nombre,simbolo));
		}		
		juego(filas, columnas, jugadores);
	}
	
	/**
	 * Modera el juego entre jugadores
	 */
	public void juego(int numeroFilas , int numeroCol , ArrayList <Jugador> pJugadores)
	{
		//TODO
		
		//inicializa juego
		System.out.println("");
		System.out.println("Empezemos!!!");
		
		juego = new LineaCuatro(jugadores, numeroFilas, numeroCol);
				
		//inicia juego
		while (true)
		{			
			System.out.println("");
			System.out.println("Turno de: "+juego.darAtacante());
			imprimirTablero();			
			while (true)
			{
				try
				{
					System.out.println("¿En que columna quieres poner la ficha?");
					int col = Integer.parseInt(sc.next());
					if(col<numeroCol)
					{
						if(juego.registrarJugada(col))
						{
							break;
						}
						else 
						{
							System.out.println("Comando inválido");
						}
					}
					else
					{
						System.out.println("Por favor ingrese un numero de columna valido");
					}
				}
				catch (NumberFormatException e)
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			if(juego.fin())break;
		}
		System.out.println("");
		System.out.println("fin de juego");
		imprimirTablero();
		if(juego.darVictoria())System.out.println("Victoria de "+juego.darUltimo()+"!!");
		if(!juego.tableroConEspacio())System.out.println("No hay mas espacio");	
	}
	
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		//TODO
		
		//valores inicilas
		int filas = 0;
		int columnas = 0;
		String nombre = "incial";
		String simbolo = "inicial";
				
		//pide filas
		while (true)
		{
			try
			{
				System.out.println("");
				System.out.println("¿Numero de filas (minimo 4)?");
				filas = Integer.parseInt(sc.next());
				if(filas>3)break;
			}
			catch (NumberFormatException e)
			{
				System.out.println("Por favor ingrese un numero de filas valido");
				continue;
			}
		}
		
		//pide columnas
		while (true)
		{
			try
			{
				System.out.println("");
				System.out.println("¿Numero de columnas (minimo 4)?");
				columnas = Integer.parseInt(sc.next());
				if(columnas>3)break;
			}
			catch (NumberFormatException e)
			{
				System.out.println("Por favor ingrese un numero de columnas valido");
				continue;
			}
		}

		//pide nombre
		while (true)
		{
			try
			{
				System.out.println("");
				System.out.println("¿Que nombre quiere usar (difernte a maquina)?");
				nombre = sc.next();
				if(!nombre.equals("maquina"))break;
			}
			catch (NumberFormatException e)
			{
				System.out.println("por favor igrese un nombre valido difernte a maquina");
				continue;
			}
		}

		//pide simbolo
		while (true)
		{
			try
			{
				System.out.println("");
				System.out.println("¿Que simbolo desea usar (numero de un digito o una letra difernte de m)?");
				simbolo = sc.next();
				if(!simbolo.equals("m") && 
						(esMayuscula(simbolo) || esMinuscula(simbolo) ||
						esNumeroUnDigito(simbolo))
						)break;
			}
			catch (NumberFormatException e)
			{
				System.out.println("Por favor, seleccione un simbolo diferente a m");
				continue;
			}
		}		
		
		//inicia juego
		juegoMaquina(filas, columnas, nombre, simbolo);
				
	}
	
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina( int numeroFilas, int numeroColumnas, String nombreJugador, String simboloJugador)
	{
		//TODO
		
		//inicializa juego
		jugadores = new ArrayList<Jugador>();
		jugadores.removeAll(jugadores);
		jugadores.add( new Jugador(nombreJugador, simboloJugador));
		String nombreMaquina = "maquina";
		String simboloMaquina = "m";
		jugadores.add( new Jugador(nombreMaquina,simboloMaquina));
		juego = new LineaCuatro(jugadores, numeroFilas, numeroColumnas);
		System.out.println("");
		System.out.println("Empezemos!!!");
				
		//inicia juego
		while (true)
		{
			
			//turno maquina
			if(juego.darAtacante().equals(nombreMaquina))
			{
				juego.registrarJugadaAleatoria();
			}
			
			//turno jugador
			else
			{

				System.out.println("");
				System.out.println("Turno de: "+juego.darAtacante());
				imprimirTablero();
				
				while (true)
				{
					try
					{
						System.out.println("¿En que columna quieres poner la ficha?");
						int col = Integer.parseInt(sc.next());
						if(col<numeroColumnas)
						{
							if(juego.registrarJugada(col))
							{
								break;
							}
							else 
							{
								System.out.println("Comando inválido");
							}
						}
						else
						{
							System.out.println("Por favor ingrese un numero de columna valido");
						}
					}
					catch (NumberFormatException e)
					{
						System.out.println("Comando inválido");
						continue;
					}
				}
			}	
			if(juego.fin())break;
		}
		imprimirTablero();
		System.out.println("fin de juego");
		if(juego.darVictoria())System.out.println("Victoria de "+juego.darUltimo()+"!!");
		if(!juego.tableroConEspacio())System.out.println("No hay mas espacio");
		
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		//TODO
		
		tab = juego.darTablero();
		String filaIntermedia = "-";
		String filaNumero =" ";
		for (int i = 0; i<tab[0].length ; i++)
		{	
			filaIntermedia += "----";
			filaNumero += " "+i+"  ";
		}
		//System.out.println("");
		System.out.println(""+filaNumero);
		System.out.println(""+filaIntermedia);
		
		for(int i=0;i<tab.length;i++)
		{
			String fila = "|";
			for(int j=0;j<tab[0].length;j++)
			{
				fila += tab[i][j] + "|";
			}
			System.out.println(""+fila);
			System.out.println(""+filaIntermedia);
		}
	}	
	
	public boolean esMinuscula(String revisar)
	{
		if(revisar.equals("a") || revisar.equals("b") || revisar.equals("c") || 
				revisar.equals("d") || revisar.equals("e") || revisar.equals("f") ||
				revisar.equals("g") || revisar.equals("h") || revisar.equals("i") ||
				revisar.equals("j") || revisar.equals("k") || revisar.equals("l") || 
				revisar.equals("l") || revisar.equals("m") || revisar.equals("n") || 
				revisar.equals("ñ") || revisar.equals("o") || revisar.equals("p") || 
				revisar.equals("q") || revisar.equals("r") || revisar.equals("s") || 
				revisar.equals("t") || revisar.equals("u") || revisar.equals("v") || 
				revisar.equals("w") || revisar.equals("x") || revisar.equals("y") || 
				revisar.equals("z")	)return true;
		return false;
	}
	
	public boolean esMayuscula(String revisar)
	{
		if(revisar.equals("A") || revisar.equals("B") || revisar.equals("C") || 
				revisar.equals("D") || revisar.equals("E") || revisar.equals("F") ||
				revisar.equals("G") || revisar.equals("H") || revisar.equals("I") ||
				revisar.equals("J") || revisar.equals("K") || revisar.equals("L") || 
				revisar.equals("L") || revisar.equals("M") || revisar.equals("N") || 
				revisar.equals("Ñ") || revisar.equals("O") || revisar.equals("P") || 
				revisar.equals("Q") || revisar.equals("R") || revisar.equals("S") || 
				revisar.equals("T") || revisar.equals("U") || revisar.equals("V") || 
				revisar.equals("W") || revisar.equals("X") || revisar.equals("Y") || 
				revisar.equals("Z")	)return true;
		return false;
	}
	
	public boolean esNumeroUnDigito(String revisar)
	{
		if(revisar.equals("0") || revisar.equals("1") || revisar.equals("2") || 
				revisar.equals("3") || revisar.equals("4") || revisar.equals("5") ||
				revisar.equals("6") || revisar.equals("7") || revisar.equals("8") ||
				revisar.equals("9") )return true;
		return false;
	}
}
